
setInterval(function() {
    if($("#tf1").is(":checked")) {
        $("#tf1").parent().css("background-color", "white");
    }
    else {
        $("#tf1").parent().css("background-color", "#f4f4f4");
    }

    if($("#tf2").is(":checked")) {
        $("#tf2").parent().css("background-color", "white");
    }
    else {
        $("#tf2").parent().css("background-color", "#f4f4f4");
    }

    if ($(".oneWay input").is(":checked")){
      $("#date_range").css("display", "none");
      $("#date_single").css("display", "inline-block");
    }
    else {
      $("#date_single").css("display", "none");
      $("#date_range").css("display", "inline-block");
    }
}, 100);

$( document ).ready(function(){

  $('.ticketForm input[type="button"]').click(function() {
      var filed1 = $(".findTickets .ticketForm input:nth-child(1)").val();
      var filed2 = $(".findTickets .ticketForm input:nth-child(3)").val();
      $(".findTickets .ticketForm input:nth-child(1)").val(filed2);
      $(".findTickets .ticketForm input:nth-child(3)").val(filed1);

  });

  $('.ticketForm .plus').click(function() {
      var value = $(this).parent().find('.value').text();
      if (value != 9){
        value = parseInt(value) + parseInt(1);
        $(this).parent().find('.value').text(value);
      }
      if (value == 9){
        $(this).css('background', 'gray');
      }
      if (value == 1) {
        $(this).parent().find('.minus').css('background', '#FF6200');
      }
  });

  $('.ticketForm .minus').click(function() {
      var value = $(this).parent().find('.value').text();
      if (value != 0){
        value = parseInt(value) - parseInt(1);
        $(this).parent().find('.value').text(value);
      }
      if (value == 0){
        $(this).css('background', 'gray');
      }
      if (value == 8) {
        $(this).parent().find('.plus').css('background', '#FF6200');
      }
  });


});

$(document).mouseup(function (e) {
    var bla = 1;
    var container = $(".findTickets .ageTicketBox");
      if (container.has(e.target).length === 0 &&
      !container.is(e.target)){
        $(".findTickets .ageTicketCheck").attr("checked", false);
      }
});

addEventListener('DOMContentLoaded', function () {
  pickmeup.defaults.locales['ru'] = {
	days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
	daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
	daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
	months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
	monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
};
  var plus_6_days = new Date;
	plus_6_days.setDate(plus_6_days.getDate() + 6);
	pickmeup('#date_range', {
    format: "d.m.Y",
    locale: "ru",
		position : 'bottom',
		mode : 'range',
    date      : [
			new Date,
			plus_6_days
		],
    hide_on_select : false,
    calendars : 2,
    select_month: false,
    select_year: false,
    min: new Date
	});

  pickmeup('#date_single', {
    format: "d.m.Y",
    locale: "ru",
		position : 'bottom',
    hide_on_select : false,
    calendars : 1,
    select_month: false,
    select_year: false,
    min: new Date
	});

});
